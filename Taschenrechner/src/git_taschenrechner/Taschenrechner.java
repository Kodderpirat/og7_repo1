package git_taschenrechner;

public class Taschenrechner {
	public int zahl1;
	public int zahl2;
	
	public double add(double zahl1, double zahl2) {
		return zahl1 + zahl2;
	}
	
	public double sub(double zahl1, double zahl2){
		return zahl1-zahl2;
	}
	
	public double mul(double zahl1, double zahl2){
		return zahl2*zahl1;
	}

	public double div(double zahl1, double zahl2){
		return zahl1/zahl2;
	}
	
}

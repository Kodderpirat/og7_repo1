package omnom;

public class Haustier {
	private int hunger =100;
	private int schlaf =100;
	private int zufriedenheit =100;
	private int gesundheit =100;
	private String name;
	
	
	public Haustier(String name) {
		this.setName(name);
	}
	public Haustier() {
	}
	public int getHunger() {
		return hunger;
	}
	public void setHunger(int hunger) {
		if (hunger<0) hunger=0;
		if (hunger>100)hunger=100;
		this.hunger = hunger;
	}
	public int getMuede() {
		return schlaf;
	}
	public void setMuede(int schlaf) {
		if (schlaf<0) schlaf=0;
		if (schlaf>100)schlaf=100;
		this.schlaf = schlaf;
	}
	public int getZufrieden() {
		return zufriedenheit;
	}
	public void setZufrieden(int zufriedenheit) {
		if (zufriedenheit<0) zufriedenheit=0;
		if (zufriedenheit>100)zufriedenheit=100;
		this.zufriedenheit = zufriedenheit;
	}
	public int getGesund() {
		return gesundheit;
	}
	public void setGesund(int gesundheit) {
		if (gesundheit<0) gesundheit=0;
		if (gesundheit>100)gesundheit=100;
		this.gesundheit = gesundheit;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void fuettern(int wert) {
		setHunger(hunger + wert);
	}
	public void schlafen(int wert) {
		setMuede(schlaf + wert);
	}
	public void spielen(int wert) {
		setZufrieden(zufriedenheit + wert);
	}
	public void heilen() {
		setGesund(100);
	}

}

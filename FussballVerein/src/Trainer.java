
public class Trainer extends Mitglied {
	private char lizenzklasse;
	private int aufwandsentschaedigung;
	
	public Trainer(String name,int telefonnummer,boolean hatBezahlt,char lizenzklasse,int aufwandsentschaedigung) {
		super(name,telefonnummer,hatBezahlt);
		this.lizenzklasse = lizenzklasse;
		this.aufwandsentschaedigung=aufwandsentschaedigung;
	}

	public char getLizenzklasse() {
		return lizenzklasse;
	}

	public void setLizenzklasse(char lizenzklasse) {
		lizenzklasse = lizenzklasse;
	}

	public int getAufwandsentschaedigung() {
		return aufwandsentschaedigung;
	}

	public void setAufwandsentschaedigung(int aufwandsentschaedigung) {
		aufwandsentschaedigung = aufwandsentschaedigung;
	}
	public String ToString(){
		return ("name: "+ name+" Telefonnummer: "+telefonnummer+" hatBezahlt: "+hatBezahlt+" lizenszklasse: "+lizenzklasse+" Aufwandsentschaedigung: "+aufwandsentschaedigung);
	}
	

}

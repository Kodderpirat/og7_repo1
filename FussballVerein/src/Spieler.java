
public class Spieler extends Mitglied {
	private int trikotnummer;
	private String spielerposition;
	public Spieler(String name,int telefonnummer,boolean hatBezahlt,int trikotnummer,String spielerposotion) {
		super(name,telefonnummer,hatBezahlt);
	}
	public int getTrikotnummer() {
		return trikotnummer;
	}
	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}
	public String getSpielposition() {
		return spielerposition;
	}
	public void setSpielposition(String spielposition) {
		spielerposition = spielposition;
	}
	
	public String ToString() {
		return ("Name: "+name+" Telefonnummer: "+telefonnummer+" Hat Bezahlt: "+hatBezahlt+" Trikotnummer: "+trikotnummer+" Spielerposition: "+spielerposition);
	}

}

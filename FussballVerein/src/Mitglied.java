
public class Mitglied {
	protected String name;
	protected int telefonnummer;
	protected boolean hatBezahlt;
	public Mitglied (String name,int telefonnummer,boolean hatBezahlt) {
		super();
		this.name= name;
		this.telefonnummer=telefonnummer;
		this.hatBezahlt=hatBezahlt;
	}
	public int getTelefonnummer() {
		return telefonnummer;
	}
	public void setTelefonnummer(int telefonnummer) {
		this.telefonnummer = telefonnummer;
	}
	public boolean isHatBezahlt() {
		return hatBezahlt;
	}
	public void setHatBezahlt(boolean hatBezahlt) {
		this.hatBezahlt = hatBezahlt;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String ToString(){
		return ("name: "+ name+" Telefonnummer: "+telefonnummer+" hatBezahlt: "+hatBezahlt);
	}
}

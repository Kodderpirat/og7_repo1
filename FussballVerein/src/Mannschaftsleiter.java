
public class Mannschaftsleiter extends Mitglied {
	private String teamname;
	
	public Mannschaftsleiter(String name,int telefonnummer,boolean hatBezahlt,String teamname) {
		super(name,telefonnummer,hatBezahlt);
		this.teamname=teamname;
	}
	
	public String getTeamname() {
		return teamname;
	}

	public void setTeamname(String teamname) {
		this.teamname = teamname;
	}
	public String ToString(){
		return ("name: "+ name+" Telefonnummer: "+telefonnummer+" hatBezahlt: "+hatBezahlt+"Team Name: "+teamname);
	}

}

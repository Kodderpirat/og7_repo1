
public class Schiedsrichter extends Mitglied {
	private int gepfiffeneSpiele;
	public Schiedsrichter(String name,int telefonnummer,boolean hatBezahlt,int gepfiffeneSpiele) {
		super(name,telefonnummer,hatBezahlt);
		this.gepfiffeneSpiele=gepfiffeneSpiele;
	}
	public int getGepfiffeneSpiele() {
		return gepfiffeneSpiele;
	}

	public void setGepfiffeneSpiele(int gepfiffeneSpiele) {
		this.gepfiffeneSpiele = gepfiffeneSpiele;
	}
	public String ToString(){
		return ("name: "+ name+" Telefonnummer: "+telefonnummer+" hatBezahlt: "+hatBezahlt+" gepfiffene Spiele: "+gepfiffeneSpiele);
	}
}

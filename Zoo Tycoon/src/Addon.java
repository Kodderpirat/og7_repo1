
public class Addon {
	private int preis;
	private int maximalAnzahl;
	private int id;
	private int anzahl;
	
	public Addon(int preis,int maximalAnzahl,int id,int anzahl){
		this.preis = preis;
		this.maximalAnzahl = maximalAnzahl;
		this.id = id;
		this.anzahl = anzahl;
	}

	public int getPreis() {
		return preis;
	}

	public void setPreis(int preis) {
		this.preis = preis;
	}

	public int getMaximalAnzahl() {
		return maximalAnzahl;
	}

	public void setMaximalAnzahl(int maximalAnzahl) {
		this.maximalAnzahl = maximalAnzahl;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	public void verbrauchen() {
		anzahl = anzahl -1;
	}
	public void kaufen() {
		anzahl = anzahl +1;
	}
	

	
}

package Felder.src;

/**
  *
  * �bungsklasse zu Feldern
  *
  * @version 1.0 vom 05.05.2011
  * @author Tenbusch
  */

public class Felder {

  //unsere Zahlenliste zum Ausprobieren
  private int[] zahlenliste = {5,8,4,3,9,1,2,7,6,0};
  
  //Konstruktor
  public Felder(){}

  //Methode die Sie implementieren sollen
  //ersetzen Sie den Befehl return 0 durch return ihre_Variable
  
  //die Methode soll die gr��te Zahl der Liste zur�ckgeben
  public int maxElement(){
    int x=0;
    int y=0;
    for (int i : zahlenliste) {
      y++;
      if(y>zahlenliste.length){
        y=i;
      }
      if(zahlenliste[i]>zahlenliste[y+1]){
        if (zahlenliste[i]>x)
        zahlenliste[i]=x;
      }
    }
    return x;
  }

  //die Methode soll die kleinste Zahl der Liste zur�ckgeben
  public int minElement(){
    int x=0;
    int y=0;
    for (int i : zahlenliste) {
      y++;
      if(y>zahlenliste.length){
        y=i;
      }
      if(zahlenliste[i]<zahlenliste[y+1]){
        if (zahlenliste[i]<x)
          x = zahlenliste[i];
      }
    }
    return x;
  }
  
  //die Methode soll den abgerundeten Durchschnitt aller Zahlen zur�ckgeben
  public int durchschnitt(){
    int x=0;
    for (int i : zahlenliste) {
      x =x+ zahlenliste[i];
    }
    x = x / zahlenliste.length;
    return x;
  }

  //die Methode soll die Anzahl der Elemente zur�ckgeben
  //der Befehl zahlenliste.length; k�nnte hierbei hilfreich sein
  public int anzahlElemente(){
    return zahlenliste.length;
  }

  //die Methode soll die Liste ausgeben
  public String toString(){
    for (int i : zahlenliste) {

    }
    return null;
  }

  //die Methode soll einen booleschen Wert zur�ckgeben, ob der Parameter in
  //dem Feld vorhanden ist
  public boolean istElement(int zahl){
    boolean x=false;
    for (int i : zahlenliste) {
      if(zahl == zahlenliste[i]){
        x=true;
      }
    }
    return x;
  }
  
  //die Methode soll das erste Vorkommen der
  //als Parameter �bergebenen  Zahl liefern oder -1 bei nicht vorhanden
  public int getErstePosition(int zahl){
    int x=0;
    for (int i : zahlenliste) {
      if(zahlenliste[i] == zahl){
        if(x==0){
          x=i;
        }
      }
    }
    return x;
  }
  
  //die Methode soll die Liste aufsteigend sortieren
  //googlen sie mal nach Array.sort() ;)
  public void sortiere(){
  }

  public static void main(String[] args) {
    Felder testenMeinerLoesung = new Felder();
    System.out.println(testenMeinerLoesung.maxElement());
    System.out.println(testenMeinerLoesung.minElement());
    System.out.println(testenMeinerLoesung.durchschnitt());
    System.out.println(testenMeinerLoesung.anzahlElemente());
    System.out.println(testenMeinerLoesung.toString());
    System.out.println(testenMeinerLoesung.istElement(9));
    System.out.println(testenMeinerLoesung.getErstePosition(5));
    testenMeinerLoesung.sortiere();
    System.out.println(testenMeinerLoesung.toString());
  }
}
